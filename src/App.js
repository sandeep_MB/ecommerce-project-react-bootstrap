import "./App.css";
import React, { Component } from "react";

import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Carousel from "./components/Carousel/Carousel";
import Footer from "./components/Footer/Footer";
import ProductPage from "./components/ProductPage/ProductPage";
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Carousel />
        <Main />
        <ProductPage />
        <Footer />
      </div>
    );
  }
}

export default App;
