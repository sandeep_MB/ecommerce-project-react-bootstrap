import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg fixed-top bg-dark navbar-dark mb-5 py-2">
          <div className="container">
            <a href="#" className="navbar-brand">
              Shoppers Stop
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navmenu"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navmenu">
              <ul className="navbar-nav ms-auto">
                <li className="nav-item">
                  <a href="#" className="nav-link">
                    About us
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#" className="nav-link">
                    Products
                  </a>
                </li>
                <li className="nav-item">
                  <a href="#" className="nav-link">
                    Contact Us
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
