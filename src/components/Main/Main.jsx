import React, { Component } from "react";

class Main extends React.Component {
  render() {
    return (
      <div>
        <section className="py-4 bg-dark text-light text-center text-sm-start">
          <div className="container text-center">
            <h1 className="py-2">
              Shop at <span className="text-warning"> Shoppers Stop </span>
            </h1>
            <button
              className="btn btn-danger btn-lg"
              data-bs-toggle="modal"
              data-bs-target="#enroll"
            >
              Shop
            </button>
          </div>
        </section>
      </div>
    );
  }
}

export default Main;
