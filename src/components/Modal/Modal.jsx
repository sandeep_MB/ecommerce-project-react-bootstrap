import React, { Component } from "react";

export default class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: this.props.products[this.props.id - 1],
      error: false,
    };
  }

  // displayProduct(id) {
  //   const product = this.props.products.filter((p) => p.id === id);

  // }

  render() {
    console.log(this.state.product);
    console.log(this.props.id);
    return (
      <div>
        <div>
          <button
            type="button"
            className="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            View
          </button>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">
                    Display Product
                  </h5>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <div>
                    <img
                      className="w-50 h-50 bd-placeholder-img card-img-top py-4"
                      height="225"
                      src={this.state.product.image}
                      aria-label="Placeholder: Thumbnail"
                    />
                  </div>
                  <div>
                    <p>Title:- {this.state.product.title}</p>
                    <p>Price:- {this.state.product.price}</p>
                    <p>Description:- {this.state.product.description}</p>
                    <p>Category:- {this.state.product.category}</p>
                  </div>
                </div>
                {/* ) : undefined} */}
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
