import React, { Component } from "react";
import DeleteProduct from "../DeleteProduct/DeleteProduct";
import UpdateProduct from "../UpdateProduct/UpdateProduct";
import Loader from "../Loader/Loader";
// import Modal from "../Modal/Modal";

class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      error: false,
      loader: true,
      deleted: false,
    };
  }

  getApi(url) {
    fetch(url)
      .then((res) => res.json())
      .then((data) =>
        this.setState({ products: data, error: true, loader: false })
      )
      .catch((err) => console.error(err));
  }

  deleteProduct(id) {
    // const productArray = this.state.products.filter((p) => p.id != id);
    // this.setState({ products: productArray, deleted: true });
    // console.log("Deleted");
    fetch("https://fakestoreapi.com/products/" + id, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const productArray = this.state.products.filter((p) => p.id != data.id);
        console.log(productArray);
        this.setState({ products: productArray, deleted: true });
      })
      .catch((err) => console.error(err));
  }

  updateProduct(id) {
    console.log(id);
    fetch(
      fetch("https://fakestoreapi.com/products/" + id, {
        method: "PATCH",
        body: JSON.stringify({
          title: "test product",
          price: 13.5,
          description: "lorem ipsum set",
          image: "https://i.pravatar.cc",
          category: "electronic",
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          const index = this.state.products.findIndex((product) => {
            return id == product.id;
          });
          console.log(`index ${index}`);
          this.setState((this.state.products[index] = data));
        })
        .catch((error) => {
          console.log(error);
        })
    );
  }

  addProduct() {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      body: JSON.stringify({
        title: "test product",
        price: 13.5,
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const array = this.state.products;
        array.push(data);
        this.setState({ products: array });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getApi("https://fakestoreapi.com/products/");
    setTimeout(() => {
      this.addProduct();
    }, 1000);
  }

  render() {
    const { products, error, loader } = this.state;

    if (loader) {
      return <Loader />;
      // <div className="spinner-border text-primary" role="status">
      //   <span className="visually-hidden">Loading...</span>
      // </div>
    }
    return (
      <section className="p-5">
        {error ? (
          <div className="album py-5 bg-light px-3">
            {/* <div className="container text-center"> */}
            <h3 className="mb-4">
              <span className="mx-2 badge bg-primary">New</span>Arrivals
            </h3>
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {/* Column */}
              {products.map((product, index) => {
                return (
                  <div key={product.id} className="col">
                    <div className="h-100 card shadow-sm d-flex justify-content-center align-items-center">
                      {index === products.length - 1 ? (
                        <h3>
                          <span className="mx-2 badge bg-primary">New</span>
                        </h3>
                      ) : undefined}
                      <img
                        className="w-50 h-50 bd-placeholder-img card-img-top py-4"
                        height="225"
                        src={product.image}
                        aria-label="Placeholder: Thumbnail"
                      />
                      <p
                        className="mt-4"
                        x="50%"
                        y="50%"
                        fill="#eceeef"
                        dy=".3em"
                      >
                        <i className="bi bi-tags-fill"></i>$ {product.price}
                      </p>

                      <div className="card-body d-flex justify-content-between flex-column">
                        <p className="card-text">
                          <strong>{product.title}</strong>
                        </p>
                        <div className="d-flex justify-content-center align-items-center">
                          <div className="btn-group">
                            <button
                              type="button"
                              className="btn btn-sm btn-outline-dark"
                            >
                              <i className="px- fas fa-cart-arrow-down fa-2x"></i>
                            </button>
                            {/* <button
                              type="button"
                              className="btn btn-sm btn-outline-dark"
                              onClick={() => {
                                this.updateProduct(product.id);
                              }}
                            > */}
                            <UpdateProduct
                              updateProduct={() =>
                                this.updateProduct(product.id)
                              }
                              id={product.id}
                            />
                            <DeleteProduct
                              deleteProduct={() =>
                                this.deleteProduct(product.id)
                              }
                              id={product.id}
                            />

                            {/* <button data-bs-target="#modal">View</button>
                            <div id="modal">
                              <Modal id={product.id} products={products} />
                            </div> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        ) : undefined}
      </section>
    );
  }
}

export default ProductPage;
