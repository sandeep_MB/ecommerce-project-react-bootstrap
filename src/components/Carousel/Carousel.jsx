import React, { Component } from "react";

class Carousel extends Component {
  render() {
    return (
      <div>
        <div
          id="carouselExampleControls"
          className="mt-5 carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="https://sslimages.s3-ap-southeast-1.amazonaws.com/sys-master/root/h9d/h57/26786820358174/men_web_newhouse122.jpg"
                className="d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://sslimages.shoppersstop.com/sys-master/root/he1/h22/26847511937054/Top-Carousals-revised20220116PVT-Brand-Web.jpg"
                className="d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://sslimages.shoppersstop.com/sys-master/root/h59/he3/26835249692702/Top-Carousals-DW-Web_20220113.jpg"
                className="d-block w-100"
                alt="..."
              />
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    );
  }
}

export default Carousel;
