import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer className="p-5 bg-dark text-white text-center position-relative">
        <div className="container">
          <p className="lead">Copyright &copy; 2021 shoppers Stop</p>

          <a href="#" className="position-absolute bottom-0 end-0 p-5">
            <i className="bi bi-arrow-up-circle h1"></i>
          </a>
          <div>
            <i className="p-4 bi bi-twitter text-light"></i>

            <i className="p-4 bi bi-facebook text-light"></i>

            <i className="p-4 bi bi-linkedin text-light"></i>

            <i className="p-4 bi bi-instagram text-light"></i>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
