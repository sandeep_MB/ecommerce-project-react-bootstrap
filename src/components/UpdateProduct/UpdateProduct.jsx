import React, { Component } from "react";

export default class UpdateProduct extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="btn-group">
        <button
          type="button"
          className="btn btn-sm btn-outline-dark"
          onClick={this.props.updateProduct}
        >
          Update
        </button>
      </div>
    );
  }
}
